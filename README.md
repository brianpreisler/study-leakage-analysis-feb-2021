# README #


### What is this repository for? ###

* Allows for analysis to be done in an ordered way on a study attendances dataset
* Version: Python 3
* Cookiecutter template is the backbone, but future work could include modifying and owning a QGI cookiecutter template

### Contribution guidelines ###

* Make sure to create a 'pull request' that I can view/approve.  I will then merge changes into master

### For future modeling of analyses after this project:

1.Create new repository on Bit bucket

2.Clone it into a local repo

3.cd into that repo

4.cookiecutter https://github.com/chris1610/pbp_cookiecutter
    -It asks if you want to delete and redownload, say NO!

5.place raw data in raw data folder (make sure it's a CSV or edit the read_csv part of the Python script
   -probably will need to add "encoding = 'latin-1'" into read_csv line

6.Checkout new branch

7.Open 1-Data Prep ipynb file

8.Move on to 2-EDA when complete with above steps

9.copy git ignore file and put in root folder with ipynb files

10.add/commit/push changes up